# API mock

## Commands
```shell
# Run api mock server (for API request simulator)
# For {api:mock:lan} you need the file {<project-root>/api-mock/.host} with your IP 192.168.x.x
npm run api:mock:{local|lan}
```
