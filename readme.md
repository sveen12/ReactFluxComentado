## Libraries/tools

This project uses libraries and tools like:
- es6 syntax and [babel](https://babeljs.io)
- [react](https://facebook.github.io/react) for the Website App and Desktop App,
- [Electron](http://electron.atom.io) to package the Desktop App
- [flux](https://facebook.github.io/flux) to organize the data flow management
- [grunt](http://gruntjs.com) to create the builds
- [webpack](https://webpack.github.io) to help during the development phase with hot reloading

### Flux architecture actions/stores

All the [flux](https://facebook.github.io/flux) architecture is share to 100% to all the different builds. This means that all the logic and data management code is done only once and reuse everywhere. This allows us to have an easy tests suite as well and to ensure that our code is working properly on all the devices.

# How to build/run the projects

## General requirements before running any specific project

- Install [NodeJS 6.x +](https://nodejs.org/en/download/package-manager/) with NPM
- Install [Yarn](https://yarnpkg.com/)
- Run `yarn` to install all the dependencies, React and React Native among others.

## The Website App

### Requirements for React

There isn't any addtional requirements since you already installed the deps with `yarn`.

### Quick start

- `npm run web-desktop:build` to build the project (at least the first time)
- `npm run web:serve` to preview in the browser at http://localhost:8000/index.web.html or http://localhost:8000/webpack-dev-server/index.web.html with webpack-dev-server and hot reload enabled

Congratulations! You've just successfully run the project as a Website App.

## The Desktop App

### Quick start with Electron

- `npm run web-desktop:build` to build the project (at least the first time)
- `npm run desktop:serve` to launch the desktop app and enable livereload

Congratulations! You've just successfully run the project as a Desktop App.

# Info
[API docs](api-mock/README.md)

# Troubleshooting
- #1
    - Problem: `Unhandled 'error' event with json-server --watch`
    - Cause: `npm run api:mock:x`
    - Solution: On linux, run `echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p`
    - Ref: [GitHub issue](https://github.com/typicode/json-server/issues/361)
- #2
    - Problem: Update NodeJS & NPM versions
    - Solution: On linux, run `sudo apt-get purge nodejs npm`, then install [6.x](https://nodejs.org/en/download/package-manager/)

- #3
    - Change [Android package](https://saumya.github.io/ray/articles/72/)

# Commands

```shell
#--------------------------
# DEVELOPMENT
#--------------------------

# Run api mock server (for API request simulator)
# For {api:mock:lan} you need the file {<project-root>/api-mock/.host} with your IP 192.168.x.x
npm run api:mock:{local|lan}

#--------------------------
# RUN
#--------------------------

# Build web and desktop base
npm run web-desktop:build

# Serve web
npm run web:serve

# Serve desktop
npm run desktop:serve


#--------------------------
# PACKAGING WEB & DESKTOP
#--------------------------

# Serve web dist
npm run web:serve:dist

# Package desktop (linux x86)
npm run desktop:package:linux:86

# Package desktop (linux x64)
npm run desktop:package:linux:64

# Package desktop (windows x86)
npm run desktop:package:windows:86

# Package desktop (windows x64)
npm run desktop:package:windows:64

# Package desktop (all platforms and architectures)
npm run desktop:package:all
```
