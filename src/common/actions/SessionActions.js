import AppDispatcher from '../dispatcher/AppDispatcher';
import SessionConstants from '../constants/SessionConstants';

const SessionActions = {

    /*
    Segundo paso: Se hace el dispatch (Se le anuncia a todos los Stores suscritos que ocurrio algo
     */
    login: (username, password) =>    
    {
        AppDispatcher.dispatch({
            type: SessionConstants.LOGIN,
            user: {
                username,
                password
            }
        });
    },

    logout: () =>    
    {
        AppDispatcher.dispatch({
            type: SessionConstants.LOGOUT
        });
    }

};

export default SessionActions;
