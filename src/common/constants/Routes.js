const Routes = Object.freeze({
    LOGIN: 'login',
    HOME: 'home'
});

export default Routes;