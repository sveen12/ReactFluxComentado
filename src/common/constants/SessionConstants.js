import keyMirror from 'keymirror';

const SessionConstants = keyMirror({
    LOGIN: null,
    FAIL_LOGIN: null,
    SET: null,
    LOGOUT: null
});

export default SessionConstants;