import ServiceConstants from '../constants/ServiceConstants';
import Http from 'axios';
import HttpQueryBuilder from 'querystring';

class SessionService {

    /**
     * User login
     * @param username
     * @param password
     * @returns {Promise.<T>|Promise<R>}
     */
    login(username, password)
    {
        return Http.get(ServiceConstants.API_URL + 'users/login?' + HttpQueryBuilder.stringify({
            username: username,
            password: password
        }), {
            method: 'get',
            headers: {
                'Accept': 'application/json'
            }
        }).then((response) =>
        {
            return Promise.resolve(response.data);
        }).catch((error) =>
        {
            return Promise.reject(error);
        });
    }
}

export default new SessionService();
