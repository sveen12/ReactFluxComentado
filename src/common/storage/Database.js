import lodash from 'lodash';
import low from 'lowdb';

let database = null;

class Database {

    get()
    {
        if (database == null)
        {
            database = low('dbapp');
            database.defaults({posts: [], user: {}}).value();
            return database;
        }
        else
        {
            return database;
        }
    }
}

export default new Database();
