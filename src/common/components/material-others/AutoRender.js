/**
 * Created by Lenovo on 23/03/2017.
 */
import React from 'react';
import AutoComplete from 'material-ui/AutoComplete';

export default {
    main(props, state)
    {
        return (
            <div>
                <AutoComplete
                    hintText="Type anything"
                    dataSource={this.state.dataSource}
                    onUpdateInput={this.handleUpdateInput}/>
                <AutoComplete
                    hintText="Type anything"
                    dataSource={this.state.dataSource}
                    onUpdateInput={this.handleUpdateInput}
                    floatingLabelText="Full width"
                    fullWidth={true}/>
            </div>
        );
    }
}
