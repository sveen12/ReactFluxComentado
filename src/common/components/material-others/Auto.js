import Render from './AutoRender';
import {Component} from 'react';

export default class Auto extends Component {
    constructor(props)
    {
        super(props);

        this.state = {
            dataSource: [],
        };

        this.handleUpdateInput = (value) => {
            this.setState({
                dataSource: [
                    value,
                    value + value,
                    value + value + value,
                ],
            });
        };
    }

    render()
    {
        return Render.main.call(this, this.props, this.state);
    }
}
