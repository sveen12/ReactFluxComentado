import Render from './LoginRender';
import {Component} from 'react';
import SessionStore from '../../stores/SessionStore';
import Routes from '../../constants/Routes';

export default class Login extends Component {

    constructor(props)
    {
        super(props);

        this.state = {
            username: '',
            password: ''
        };

        /*
        Para otros emit se replica lo siguiente
         */
        this.onSessionChange = this.onSessionChange.bind(this);
    }
    /*
    Aqui Se suscribe ante los store  Cuando se renderizo con el callback onSessionChange
     */

    componentWillMount()
    {
        SessionStore.addChangeListener(this.onSessionChange);
    }

    /*
    Aqui Se sucribe ante los store Cuando se desrenderizo
     */

    componentWillUnmount()
    {
        SessionStore.removeChangeListener(this.onSessionChange);
    }

    /*
    Septimo: Cuando se recibe el emitChange(CHANGE_EVENT) del store se verifica que se cambio despues de que se trato la accion
     */
    onSessionChange()
    {
        if (SessionStore.loggedIn() === true)
        {
            // TODO: Go to home
        }
        else
        {
            Render.loginFailedMessage.call(this);
        }
    }

    render()
    {
        return Render.main.call(this, this.props, this.state);
    }
}
