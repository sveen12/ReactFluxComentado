import React from 'react';
import {Link} from 'react-router';
import Routes from '../../constants/Routes';
import SessionActions from '../../actions/SessionActions';

export default {
    main()
    {
        let onUserChange = (event) =>
        {
            this.setState({username: event.target.value});
        };

        let onPasswordChange = (event) =>
        {
            this.setState({password: event.target.value});
        };

        return (
            <div>
                Login placeholder <br/>

                <Link to={Routes.HOME}>Ir a home</Link> <br/>

                <input
                    placeholder='Usuario'
                    onChange={(e) => onUserChange(e)}/>

                <input
                    placeholder='Clave'
                    onChange={(e) => onPasswordChange(e)}/>

                /*
                 *Primer paso: Se dispara el actionCreator
                 */
                <button onClick={() => SessionActions.login(this.state.username, this.state.password)}>
                    Login
                </button>
            </div>
        );
    },

    loginFailedMessage()
    {
        alert('Login failed');
    }
}
