/**
 * Created by Lenovo on 23/03/2017.
 */
import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import {Link} from 'react-router';

export default {
    main(props, state)
    {
        return (
            <div>
                <Link to="/auto">Ir a auto</Link>
                <RaisedButton label="Default" style={style} />
                <RaisedButton label="Primary" primary={true} style={style} />
            </div>
        );
    }
}

const style = {
    margin: 12,
};

