import React, {Component} from 'react';
import Render from './AutoCompleteExampleSimpleRender';

/**
 * The input is used to create the `dataSource`, so the input always matches three entries.
 */
export default class AutoCompleteExampleSimple extends Component {
    constructor(props)
    {
        super(props);
    }

    render() {
        return Render.main.call(this, this.props, this.state);
    }
}