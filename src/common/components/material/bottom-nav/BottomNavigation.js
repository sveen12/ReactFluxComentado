/**
 * Created by Lenovo on 23/03/2017.
 */
import React, {Component} from 'react';
import BottomNavigationRender from './BottomNavigationRender';

/*
Dentro del constructor va el estado inicial
 */
export default class BottomNavigation extends Component {
    constructor(props){
        super(props);

        this.state = {
            selectedIndex: 0,
        };

    }



    render()
    {
        return BottomNavigationRender.main.call(this, this.props, this.state);
    }
}

/*
Se exporta ya que se llamara en el index
 */

