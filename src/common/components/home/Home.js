import Render from './HomeRender';
import {Component} from 'react';

export default class Home extends Component {
    render()    
    {
        return Render.main.call(this, this.props, this.state);
    }
}
