import {EventEmitter} from 'events';
import AppDispatcher from '../dispatcher/AppDispatcher';
import SessionConstants from '../constants/SessionConstants';
import SessionService from '../services/SessionService';

const CHANGE_EVENT = 'change';
let profile = {};
let loggedIn = false;

class SessionStore extends EventEmitter {

    constructor()
    {
        super();
        /*
         * Tercero: Se registra el Store ante el dispatcher con el callback "dispatcherCallback"
         * En este callback se hace el filtrado cuando se recibe el dispatch.
         */
        this.dispatchToken = AppDispatcher.register(this.dispatcherCallback.bind(this));
    }

    /**
     * Store emit change
     *
     * Sexto: se avisa a las vistas registradas que un cambio de tipo "CHANGE_EVENT" acaba de ocurrir
     */
    emitChange()
    {
        this.emit(CHANGE_EVENT);
    }

    /**
     * Change listener
     * @param callback
     */
    addChangeListener(callback)
    {
        this.on(CHANGE_EVENT, callback);
    }

    /**
     * Remove change listener
     * @param callback
     */
    removeChangeListener(callback)
    {
        this.removeListener(CHANGE_EVENT, callback);
    }

    /**
     * Get logged in profile
     * @returns {{userId: string, name: string, token: string}}
     */
    getProfile()
    {
        return profile;
    }

    /**
     * Get if the user is logged in
     * @returns {boolean}
     */
    loggedIn()
    {
        return loggedIn;
    }

    /**
     * Login a user
     * @param username
     * @param password
     * @returns {Promise<R>|Promise.<T>}
     *
     * Quinto: Se ejecuta la accion que actiende el tipo de accion
     */
    login(username, password)
    {
        return SessionService.login(username, password)
            .then((data) =>
            {
                profile = {
                    id: data.id,
                    name: data.name,
                    token: data.token
                };

                loggedIn = true;

                return Promise.resolve();
            })
            .catch((error) =>
            {
                return Promise.reject(error);
            });
    }

    /**
     * User logout
     */
    logout()
    {
        profile = {};
        loggedIn = false;
    }

    /**
     * Dispatcher callbacks
     * @param action
     */
    /*
     * Cuarto: se decide que hacer dependiendo del tipo de accion
     * En este callback se hace el filtrado cuando se recibe el dispatch.
     * Una vez hecho el filtrado e identificado el tipo de accion se procede a ejecutar lo debido.
     */
    dispatcherCallback(action)
    {
        switch (action.type)
        {
            case SessionConstants.LOGIN:
                this.login(action.user.username, action.user.password)
                    .then(() =>
                    {
                        /*
                         * Sexto: Si todo salio bien se avisa que se cambio por medio de un emitChange
                         */
                        this.emitChange();
                    })
                    .catch((error) =>
                    {
                        this.emitChange();
                    });

                break;
            case SessionConstants.LOGOUT:
                this.logout();
                this.emitChange();

                break;
            default:
            // No op
        }
    }
}

export default new SessionStore();
