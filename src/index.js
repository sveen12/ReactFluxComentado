import React from 'react';
import ReactDOM from 'react-dom';
import Database from './common/storage/Database';
import {Router, Route, hashHistory} from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Login from './common/components/login/Login';
import Home from './common/components/home/Home';
import AutoCompleteExampleSimple from './common/components/material/AutoCompleteExampleSimple';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Auto from './common/components/material-others/Auto';
import BottomNavigation from './common/components/material/bottom-nav/BottomNavigation';


// CSS
require('normalize.css');
require('./styles/main.css');

/* TODO: REMOVE DB - TESTING */
const db = Database.get();

db.defaults({ posts: [], user: {} })
    .value();

db.get('posts')
    .push({ id: 2, title: 'lowdb is !!!!'})
    .value();

db.set('user.name', 'michael')
    .value();
/* TODO: REMOVE DB - TESTING */


injectTapEventPlugin();

ReactDOM.render((
    <MuiThemeProvider>
        <Router history={hashHistory}>
            <Route path='/button' component={AutoCompleteExampleSimple}/>
            <Route path='/auto' component={Auto}/>
            <Route path='/' component={BottomNavigation}/>

        </Router>
    </MuiThemeProvider>
), document.getElementById('content'));
